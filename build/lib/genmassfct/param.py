"""
External Parameters
"""

class Bunch(object):
    """
    translates dic['name'] into dic.name 
    """

    def __init__(self, data):
        self.__dict__.update(data)


def cosmo_par():
    par = {
        "zmin": 6.0,    #min redshift
        "zmax": 20.0,   #max redshift
        "Nz": 10,       #Nb of redshift bins
        "zbin": 'lin',  #lin or log
        "Om": 0.315,
        "Ob": 0.049,
        "s8": 0.83,
        "h0": 0.673,
        "ns": 0.963,
        "rhoc": 2.775e11, #critical density at z=0 [[h^2 Msun/Mpc^3] 
        }
    return Bunch(par)


def constant():
    par = {
        "G": 4.299e-9,    #Newton's constant [Mpc/Msun*(km/s)^2]
        "Deltavir": 200,  #overdensity criterion
        }
    return Bunch(par)


def mf_par():
    par = {
        "dc": 1.675,  #delta_c
        "p": 0.3,     #p parameter of f(nu)
        "q": 1.0,     #q parameter of f(nu)
        "c": 2.5,     # prop constant for mass 
        }
    return Bunch(par)

def sfe_par():
    par = {
        "fstar0": 0.05,
        "Mpivot": 2.0e11,
        "gamma1": 0.49,
        "gamma2": -0.61,
        "Mtrunc": 0.0,
        "gamma3": 2.0,
        "gamma4": -1.0,
        }
    return Bunch(par)

def io_files():
    par = {
        "psfct": 'CDM_PLANCK_pk.dat',
        "varfct": 'var.dat',
        "mf_table": 'mf_table.dat',
        }
    return Bunch(par)

def code_par():
    par = {
        "rmin": 0.002,
        "rmax": 25,
        "Nrbin": 100,
        "bias": 'ellipsoidal', # [ellipsoidal/spherical/tinker/jing]
        }
    return Bunch(par)

def window_par():
    par = {
        "window": 'sharpk',
        "beta": 4.0,
        }
    return Bunch(par)

def par():
    par = Bunch({
        "cosmo": cosmo_par(),
        "mf": mf_par(),
        "window": window_par(),
        "file": io_files(),
        "code": code_par(),
        "constant": constant(),
        "sfe": sfe_par()
        })
    return par

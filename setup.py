from setuptools import setup


def readme():
    with open('README.rst') as f:
        return f.read()


genmassfct_link = 'https://bitbucket.org/aurel..'

setup(name='genmassfct',
      version='0.1',
      description='Generalised EPS halo mass function for linear power spectra with arbitrary shape',
      url=genmassfct_link,
      author='Aurel Schneider',
      author_email='schneider.duhem@gmail.com',
      packages=['genmassfct'],
      zip_safe=False)

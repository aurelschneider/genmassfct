############################################################################
#
# MASSFCT BASED ON THE SHARP-K FILTER DESIGNED TO SCOPE WITH ARBITRARY 
# INITIAL POWER SPECTRA.
#
# PRODUCES LOOK-UP TAQBNLE FOR ARES
#
# THIS VERSION: MARCH 2018
#
##############################################################################

from argparse import ArgumentParser
from math import *
from scipy.interpolate import interp1d, splrep,splev
from scipy.integrate import quad, dblquad, simps,cumtrapz, trapz
from scipy.optimize import newton, fsolve, brentq,curve_fit
from scipy.special import erf
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})
from matplotlib import text
import matplotlib.ticker as pltticker
from random import gauss
import operator
from numpy import linspace, logspace, exp, array, random
from numpy import *
import pickle

##############################################################################

parser=ArgumentParser(description='HALO MASS FUNCTION FOR ARBITRARY INITIAL POWER SPECTRA')
parser.add_argument('fileCDM', type=str, action="store", help='output CDM file')
parser.add_argument('fileADM', type=str, action="store", help='output ADM file')

parser.add_argument('-TF_CDM', type=str, action='store', help='Name of CAMB-file for CDM transfct')
parser.add_argument('-TF_ADM', type=str, action='store', help='Name of CAMB-file for alternative DM transfct')
parser.add_argument('-var',action='store_true', default=False, help='Read in var.txt [default: var.txt is created]')

parser.add_argument('-Om', type=float, action="store", default=0.304, help='Omega_matter [default 0.304]')
parser.add_argument('-Ol', type=float, action="store", default=0.696, help='Omega_lambda [default 0.696]')
parser.add_argument('-Ob', type=float, action="store", default=0.048, help='Omega_barions [default: 0.048]')
parser.add_argument('-sigma8', type=float, action="store", default=0.829, help='sigma8 [default 0.829]')
parser.add_argument('-nspec', type=float, action="store", default=0.9635, help='nspec [default 0.963]')
parser.add_argument('-hubble', type=float, action="store", default=0.681, help='hubble [default 0.681]')
parser.add_argument('-dc', type=float, action="store", default=1.675, help='delta_c [default 1.675]')
parser.add_argument('-q', type=float, action="store", default=1.0, help='q parameter of f(nu) [default 1.0]')
parser.add_argument('-c', type=float, action="store", default=2.5, help='proportionality constant of mass assignment [default 2.5]')

parser.add_argument('-zmin', type=float, action="store", default=4.0, help='Min redshift [default 6.0]')
parser.add_argument('-zmax', type=float, action="store", default=80.0, help='Max redshift [default 50.0]')
parser.add_argument('-dz', type=float, action="store", default=0.05, help=' Redshift bins [default 0.05]')

#parser.add_argument('-logMmin', type=float, action="store", default=5, help='Min mass in log10 [default 5]')
#parser.add_argument('-logMmax', type=float, action="store", default=15, help='Max mass in log10 [default 15]')
#parser.add_argument('-dlogM', type=float, action="store", default=0.1, help='logM bins [defaault 0.1]')

args = parser.parse_args()
print parser.parse_args()

Om       = args.Om
Ol       = args.Ol
Ob       = args.Ob
Ocdm     = Om-Ob
sigma8   = args.sigma8
nspec    = args.nspec
h        = args.hubble
dc       = args.dc
cc       = args.c

G        = 4.299e-9             # [Mpc/Msun*(km/s)^2]
rhoc     = 2.776e11		# comoving crit density



#f-factor: f = Ob/Om and (1-f) = Oc/Om
f = Ob/Om

#resolution limits
kmin = 0.01
kmax = 500 
rmin = 0.002
rmax = 25.0

if (kmax*rmin<=1):
    print "WARNING: kmax*rmin should be larger than one!"
if (kmin*rmax>=1):
    print "WARNING: kmin*rmax should be smaller than one!"

#Delta200 = 200
#Omz      = Om * a**(-3.0)/(Om*a**(-3.0) + Ol)
#Deltavir = (18*pi**2.0 + 82.0*(Omz-1.0)-39.0*(Omz-1.0)**2.0)/Omz
#Deltavir = 97.0/0.315
#Deltavir = 200.0
#print "Delta200, Deltavir = ", Delta200, Deltavir

# CLASSES ####################################################################

#Class of scale factor
class sf:
    Ntot = 1000
    amin = 0.0001
    amax = 1.0
    da   = (amax - amin)/Ntot
    a    = []
    D    = []

#Class of bins for var-file
class rbin:
    Ntot               = 200
    logrmin            = log(rmin)
    logrmax            = log(rmax)
    dlogr              = (logrmax - logrmin)/Ntot
    r                  = []
    mTH                = []
    mSK                = []
    dndlogmTHCDM       = []
    dndlomTHADM       = []
    dndlogmSKCDM       = []
    dndlogmSKADM       = []
    var0THCDM          = []
    var0THADM          = []
    var0SKCDM          = []
    var0SKADM          = []
    dlogvar0dlogrTHCDM = []
    dlogvar0dlogrTHADM = []
    dlogvar0dlogrSKCDM = []
    dlogvar0dlogrSKADM = []



#Class of k-bins
class kbin:
    Ntot    = 200				#number of k-bins
    logkmin = log(kmin)				#smallest k-value in log[h/Mpc]
    logkmax = log(kmax)				#largest k-value in log[h/Mpc]
    dlogk   = (logkmax - logkmin)/Ntot
    k       = []

# FUNCTIONS ##################################################################


#Window function                                                                                                        
def wf(y,window):
    if (window=='tophat'):
        w = 3.0*(sin(y) - y*cos(y))/y**3.0
        if (y>100.0):
            w = 0.0
    elif (window=='sharpk'):
        if (y <= 1.0):
            w = 1.0
        else:
            w = 0.0
    elif (window=='gaussian'):
        w = exp(-y**2.0/2.0)
    else:
        print "ERROR: undefined window function!"
        exit()
    return w

#Derivative of window function dwf = dW(kR)/dlogkR                                                                      
def dwf(y,window):
    if (window == 'tophat'):
        dw = 3.0*((y**2.0 - 3.0)*sin(y) + 3.0*y*cos(y))/y**3.0
        if (y>100.0):
            dw = 0.0
    elif (window == 'sharpk'):  #delta function (must be taken into account in main program)                            
        dw = 0.0
    elif (window == 'gaussian'):
        dw = - y**2.0*exp(-y**2.0/2.0)
    return dw

#Press-Schechter function (Cooray & Sheth eq. 57 )
def fPS(nu):
    f = sqrt(2.0*nu/pi)*exp(-nu/2.0)
    return f

#Sheth-Tormen function (Cooray & Sheth eq. 59)
def fST(nu):
    p = 0.3
    A = 0.3222
    q = 0.85#0.707
    f = A*sqrt(2.0*q*nu/pi)*(1.0 + (q*nu)**(-p))*exp(-q*nu/2.0)
    return f

#Ellipsoidal mass function (Sheth et al 1999)
def fSK(nu):
    p = 0.3
    A = 0.3222
    q = args.q
    f = A*sqrt(2.0*q*nu/pi)*(1.0 + (q*nu)**(-p))*exp(-q*nu/2.0)
    return f

#Conditional Mass function (Giocoli etal 2008, eq. 59)
def fcond(d,S,d0,S0):
    ddelta = d-d0
    dS     = S-S0
    fcond = sqrt(1.0/2.0/pi)*ddelta/dS**(3.0/2.0)*exp(-ddelta**2.0/dS/2.0)
    return fcond

#Hubble parameter in (km/s)/(Mpc/h)
def Hubble(a,Om,Ol):
    H0 = 100.0
    H  = H0 * (Om/(a**3.0) + (1 - Om - Ol)/(a**2.0) + Ol)**0.5
    return H

#Growth factor (eq. 11.56 in Longair textbook)
def Dgrowth(a,Om,Ol):
    amin  = 0.0
    amax  = a
    Ntot  = 1000
    da    = (amax - amin)/Ntot
    integral = 0.0
    for i in xrange(0,Ntot,1):
        ai = (amin + (i+1)*da + amin + i*da)/2.0
        integral += da/(ai**3.0 * Hubble(ai,Om,Ol)**3.0)
    D = Hubble(a,Om,Ol) * (5.0*Om/2.0) * integral
    return D

#Reading or loading transfer function
def TCDM():
    global TCDMb
    global TCDMc
    #reading CAMB-tf
    names='k, Tc, Tb'
    TFCDM  = genfromtxt(args.TF_CDM,usecols=(0,1,2),comments='#',dtype=None, names=names)
    print "Reading CAMB-file for CDM transfer function done!"
    TCDMc = interp1d(TFCDM['k'], TFCDM['Tc'], kind='linear')
    TCDMb = interp1d(TFCDM['k'], TFCDM['Tb'], kind='linear')

#nCDM transfer function
def TADM():
    global TADMb
    global TADMc
    #reading CAMB-tf
    names='k, Tc, Tb'
    TFADM  = genfromtxt(args.TF_ADM,usecols=(0,1,2),comments='#',dtype=None, names=names)
    print "Reading CAMB-file for ADM transfer function done!"
    TADMc = interp1d(TFADM['k'], TFADM['Tc'], kind='linear')
    TADMb = interp1d(TFADM['k'], TFADM['Tb'], kind='linear')

#Linear power spectrum
def linPS(k, model):
    if (model=='CDM'):
        linPS = ACDM * abs(f*TCDMb(k) + (1.0-f)*TCDMc(k))**2.0 * k**nspec
    elif (model=='ADM'):
        linPS = AADM * abs(f*TADMb(k) + (1.0-f)*TADMc(k))**2.0 * k**nspec
    return linPS

#tophat variance0
def var0TH_fct(r,model):
    itd = lambda logk: exp(3.0*logk) * linPS(exp(logk),model) * wf(exp(logk)*r,'tophat')**2.0
    itl = quad(itd, log(kmin), log(kmax), epsrel=5e-3, limit=1000)
    var = itl[0]/(2.0*pi**2.0)
    return var

#sharpk variance0
def var0SK_fct(r,model):
    itd = lambda logk: exp(3.0*logk) * linPS(exp(logk),model) * wf(exp(logk)*r,'sharpk')**2.0
    itl = quad(itd, log(kmin), log(kmax), epsrel=5e-3, limit=1000)
    var = itl[0]/(2.0*pi**2.0)
    return var

#tophat logarithmic derivative of variance !!! dlogvar/dlogM = -dlognu/dlogM !!!
def dlogvar0dlogrTH_fct(r, model):
    itd = lambda logk: exp(3.0*logk) * linPS(exp(logk), model) * wf(exp(logk)*r,'tophat') * dwf(exp(logk)*r,'tophat')
    itl = quad(itd, log(kmin), log(kmax), epsrel=5e-3, limit=1000)
    dlogvar0dlogr = 2.0/var0TH_fct(r,model) * itl[0]/(2.0*pi**2.0)
    return dlogvar0dlogr

#sharpk logarithmic derivative of variance !!! dlogvar/dlogM = -dlognu/dlogM !!!
def dlogvar0dlogrSK_fct(r, model):
    itd = - 0.5 * (1.0/r)**3.0 * linPS(1.0/r,model)
    dlogvar0dlogr = 2.0/var0SK_fct(r,model) * itd/(2.0*pi**2.0)
    return dlogvar0dlogr

#tophat logarithmic derivative of variance !!! dlogvar/dlogM = -dlognu/dlogM !!!
def dlogvardlogrTH_fct(r, model):
    itd = lambda logk: exp(3.0*logk) * linPS(exp(logk), model) * wf(exp(logk)*r,'tophat') * dwf(exp(logk)*r,'tophat')
    itl = quad(itd, log(kmin), log(kmax), epsrel=5e-3, limit=1000)
    dlogvardlogr = 2.0/varTH_fct(r,model) * itl[0]/(2.0*pi**2.0)
    return dlogvardlogr

#sharpk logarithmic derivative of variance !!! dlogvar/dlogM = -dlognu/dlogM !!!
def dlogvardlogrSK_fct(r, model):
    itd = - 0.5 * (1.0/r)**3.0 * linPS(1.0/r,model)
    dlogvardlogr = 2.0/varSK_fct(r,model) * itd/(2.0*pi**2.0)
    return dlogvardlogr

#tophat mass function
def dndlogmTH_fct(r,z,model):
    M = 4.0*pi*Om*rhoc*r**3.0/3.0
    Dz = splev(z,Dz_tck)
    if (model == 'CDM'):
        var0TH = splev(r,var0THCDM_tck,der=0)
        nu = dc**2.0/var0TH/Dz**2.0
        dlogvar0dlogrTH = splev(r,dlogvar0dlogrTHCDM_tck,der=0)
    elif (model == 'ADM'):
        var0TH = splev(r,var0THADM_tck,der=0)
        nu = dc**2.0/var0TH/Dz**2.0
        dlogvar0dlogrTH = splev(r,dlogvar0dlogrTHADM_tck,der=0)
    mf = - 1.0/6.0 * Om*rhoc/M * fST(nu) * dlogvar0dlogrTH
    return mf

#sharp-k massfct
def dndlogmSK_fct(r,z,model):
    M = 4.0*pi*Om*rhoc*(cc*r)**3.0/3.0
    Dz = splev(z,Dz_tck)
    if (model == 'CDM'):
        var0SK = splev(r,var0SKCDM_tck,der=0)
        nu = dc**2.0/var0SK/Dz**2.0
        dlogvar0dlogrSK = splev(r,dlogvar0dlogrSKCDM_tck)
        mf = - 1.0/6.0 * Om*rhoc/M * fSK(nu) * dlogvar0dlogrSK
    elif (model == 'ADM'):
        var0SK = splev(r,var0SKADM_tck,der=0)
        nu = dc**2.0/var0SK/Dz**2.0
        dlogvar0dlogrSK = splev(r,dlogvar0dlogrSKADM_tck)
        mf = - 1.0/6.0 * Om*rhoc / M * fSK(nu) * dlogvar0dlogrSK
    return abs(mf)

def aoft_fct(t,Om,Ol,t0):
    tL = 2.0/(3.0*(Ol)**0.5/t0)
    return (Om/Ol)**(1.0/3.0)*(sinh(t/tL))**(2.0/3.0)
    #fct = 
    #return integrate.ode()

#arXiv:1005.2416
def Tgas_fct(a,Tcmb0):
    a1 = 1.0/119.0
    a2 = 1.0/115.0
    Tgas = Tcmb0/a/(1.0+(a/a1)/(1.0+(a2/a)**(3.0/2.0)))
    return Tgas


##############################################################################
# MAIN 
##############################################################################


#Growth function
Dz0 = Dgrowth(1.0,Om,Ol)
itl = 0.0
for i in xrange(0,sf.Ntot+1,1):
    ai = sf.amin + i*sf.da
    sf.a += [ai]
    itl += sf.da/(ai**3.0 * Hubble(ai,Om,Ol)**3.0)
    sf.D += [Hubble(ai,Om,Ol) * (5.0*Om/2.0) * itl/Dz0]
sf.a = array(sf.a)
sf.D = array(sf.D)
#Da = interp1d(sf.a, sf.D, kind='linear')
#Dnorm = Da(a)
#aD = interp1d(sf.D, sf.a, kind='linear')

red = 1.0/sf.a - 1.0
Dz_tck = splrep(red[::-1],sf.D[::-1])


#load CDM transfer function
TCDM()
#load ADM transfer function
TADM()

#fill k-bins
for j in xrange(0,kbin.Ntot-1,1):
    kj = exp((kbin.logkmin + (j+1)*kbin.dlogk + kbin.logkmin + j*kbin.dlogk)/2.0)
    kbin.k += [kj]
kbin.k = array(kbin.k)

#Normalize power spectrum
R = 8.0
itdCDM = lambda logk: exp((3.0+nspec)*logk) * (f*TCDMb(exp(logk)) + (1-f)*TCDMc(exp(logk)))**2.0 * wf(exp(logk)*R,'tophat')**2.0
itlCDM = quad(itdCDM, log(kmin), log(kmax), epsrel=5e-3, limit=100)
ACDM = 2.0 * pi**2.0 * sigma8**2.0 / itlCDM[0]
itdADM = lambda logk: exp((3.0+nspec)*logk) * (f*TADMb(exp(logk)) + (1-f)*TADMc(exp(logk)))**2.0 * wf(exp(logk)*R,'tophat')**2.0
itlADM = quad(itdADM, log(kmin), log(kmax), epsrel=5e-3, limit=100)
AADM = 2.0 * pi**2.0 * sigma8**2.0 / itlADM[0] #* 0.83**2.0 #(testing maxFB)
print "Normalizing power-spectrum done!"

# CALCULATE/READ VARIANCE(S) #############################################################

#Reading or creating var-file
if (args.var==True):
    #reading var-file
    openfile  = open("varALL.txt", 'r')
    print "Reading variance from file"
    for line in openfile:
        col01, col02, col03, col04, col05, col06, col07, col08, col09 = line.split()
        rbin.r.append(float(col01))
        rbin.var0THCDM.append(float(col02))
        rbin.var0THADM.append(float(col03))
        rbin.var0SKCDM.append(float(col04))
        rbin.var0SKADM.append(float(col05))
        rbin.dlogvar0dlogrTHCDM.append(float(col06))
        rbin.dlogvar0dlogrTHADM.append(float(col07))
        rbin.dlogvar0dlogrSKCDM.append(float(col08))
        rbin.dlogvar0dlogrSKADM.append(float(col09))
else :
    out = open("varALL.txt", 'w')
    print "Calculating variance..."
    for i in xrange(0,rbin.Ntot-1,1):
        rbin.r += [exp(rbin.logrmin + (i + 0.5)*rbin.dlogr)]
        rbin.var0THCDM += [var0TH_fct(rbin.r[i], 'CDM')]
        rbin.var0THADM += [var0TH_fct(rbin.r[i], 'ADM')]
        rbin.var0SKCDM += [var0SK_fct(rbin.r[i], 'CDM')]
        rbin.var0SKADM += [var0SK_fct(rbin.r[i], 'ADM')]
        rbin.dlogvar0dlogrTHCDM += [dlogvar0dlogrTH_fct(rbin.r[i], 'CDM')]
        rbin.dlogvar0dlogrTHADM += [dlogvar0dlogrTH_fct(rbin.r[i], 'ADM')]
        rbin.dlogvar0dlogrSKCDM += [dlogvar0dlogrSK_fct(rbin.r[i], 'CDM')]
        rbin.dlogvar0dlogrSKADM += [dlogvar0dlogrSK_fct(rbin.r[i], 'ADM')]
        ratio = 100.0*float(i)/float(rbin.Ntot)
        print ratio, " %"
        print>>out, rbin.r[i], rbin.var0THCDM[i], rbin.var0THADM[i], rbin.var0SKCDM[i], rbin.var0SKADM[i], rbin.dlogvar0dlogrTHCDM[i], rbin.dlogvar0dlogrTHADM[i], rbin.dlogvar0dlogrSKCDM[i], rbin.dlogvar0dlogrSKADM[i]
print "...done!"
rbin.r = array(rbin.r)

#Interpolate TH variance
var0THCDM_tck = splrep(rbin.r, rbin.var0THCDM, s=0)
var0THADM_tck = splrep(rbin.r, rbin.var0THADM, s=0)

#interpolate SK variance
var0SKCDM_tck = splrep(rbin.r, rbin.var0SKCDM, s=0)
var0SKADM_tck = splrep(rbin.r, rbin.var0SKADM, s=0)

#Interpolate TH dlogvardlogm
dlogvar0dlogrTHCDM_tck = splrep(rbin.r, rbin.dlogvar0dlogrTHCDM, s=0)
dlogvar0dlogrTHADM_tck = splrep(rbin.r, rbin.dlogvar0dlogrTHADM, s=0)

#Interpolate SK dlogvardlogm
dlogvar0dlogrSKCDM_tck = splrep(rbin.r, rbin.dlogvar0dlogrSKCDM, s=0)
dlogvar0dlogrSKADM_tck = splrep(rbin.r, rbin.dlogvar0dlogrSKADM, s=0)



# Calculate mass function ########################################################################

mTH = 4.0*pi/3.0*Om*rhoc*rbin.r**3.0
mSK = 4.0*pi/3.0*Om*rhoc*(cc*rbin.r)**3.0
logmSK = log10(mSK)

zmin = args.zmin
zmax = args.zmax
dz   = args.dz
Nz = int((zmax-zmin)/dz)
print Nz
zz      = linspace(zmin, zmax,Nz)
Dgrowth = splev(zz,Dz_tck)
mfCDM   = []
mfADM   = []
ngtmCDM = []
ngtmADM = []
mgtmCDM = []
mgtmADM = []
for i in range(len(zz)):
     mfCDM += [dndlogmSK_fct(rbin.r,zz[i],'CDM')/mSK]
     mfADM += [dndlogmSK_fct(rbin.r,zz[i],'ADM')/mSK]
     ngtmCDM += [trapz(dndlogmSK_fct(rbin.r,zz[i],'CDM')/mSK, mSK) - cumtrapz(dndlogmSK_fct(rbin.r,zz[i],'CDM')/mSK,mSK,initial=0.0)]
     ngtmADM += [trapz(dndlogmSK_fct(rbin.r,zz[i],'ADM')/mSK, mSK) - cumtrapz(dndlogmSK_fct(rbin.r,zz[i],'ADM')/mSK,mSK,initial=0.0)]
     mgtmCDM += [trapz(dndlogmSK_fct(rbin.r,zz[i],'CDM'), mSK) - cumtrapz(dndlogmSK_fct(rbin.r,zz[i],'CDM'),mSK,initial=0.0)]
     mgtmADM +=[trapz(dndlogmSK_fct(rbin.r,zz[i],'ADM'), mSK) - cumtrapz(dndlogmSK_fct(rbin.r,zz[i],'ADM'),mSK,initial=0.0)]


savez(args.fileCDM, logM=logmSK, growth=Dgrowth, dndm=mfCDM, ngtm=ngtmCDM, mgtm=mgtmCDM, z=zz, sigma=array(rbin.var0SKCDM))
savez(args.fileADM, logM=logmSK, growth=Dgrowth, dndm=mfADM, ngtm=ngtmADM, mgtm=mgtmADM, z=zz, sigma=array(rbin.var0SKADM))


'''
npzfile = load("mf.npz")
print npzfile.files
#print npzfile['logM']

print npzfile['dndm']

print len(npzfile['z'])
print npzfile['z'][0]
'''
